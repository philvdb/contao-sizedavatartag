<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2012 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Dr. Philip von der Borch 2012
 * @author     Dr. Philip von der Borch <http://101010-webdesign.de>
 * @package    sizedAvatarTag
 * @license    LGPL
 * @filesource
 */


/**
 * Class sizedAvatar
 *
 * Implement the hook to replace an insert tag with a sized avatar
 * @copyright  Dr. Philip von der Borch 2012
 * @author     Dr. Philip von der Borch <http://101010-webdesign.de>
 * @package    sizedAvatarTag
 */
class sizedAvatar extends Controller
{

	/**
	 * Hook set for replaceInsertTags
	 */
	public function replaceTags($strTag)
	{
		$arrTag = trimsplit('::', $strTag);
		
		switch ($arrTag[0])
		{

			case 'sizedAvatar':
				if (!$arrTag[1]) 
				{
					// By default, return the currently logged in user's avatar
					if (!FE_USER_LOGGED_IN) return false;
					$this->import('FrontendUser', 'User');
					$strAvatar = $this->User->avatar;
				} 

				// We're looking for someone else's avatar
				elseif (is_numeric($arrTag[1]))
				{
					$this->import('Database');

					$strAvatar = $this->Database
						->prepare("SELECT avatar FROM tl_member WHERE id=?")
						->execute($arrTag[1])->avatar;
				}

				else return false;
				
				// Use the default image if the user has no avatar
				if (!$strAvatar || !file_exists(TL_ROOT . '/' . $strAvatar))
				{
					$strDir = trim($GLOBALS['TL_CONFIG']['avatar_dir']);
					if (!$strDir) $strDir = $GLOBALS['TL_CONFIG']['uploadPath'].'/avatars';
					$strAvatar = trim($strDir.'/default128.png');
				}				
				
				// Do some resizing if requested
				if (is_numeric($arrTag[2]))
				{
					$strAvatar = $this->getImage($strAvatar, $arrTag[2], $arrTag[3], $arrTag[4]);
				}

				return Avatar::img($strAvatar);

		} // switch

		return false;
	}
}

?>